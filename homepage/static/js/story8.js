$("input").change(function() {
    if (this.checked) {
        $('link[href="/static/css/style.css"]').attr('href', '/static/css7/style1.css');
        document.getElementById('light').innerHTML = '<strong>Night Mode?</strong>';
    } else {
        $('link[href="/static/css/style1.css"]').attr('href', '/static/css7/style.css');
        document.getElementById('light').innerHTML = '<strong>Light Mode?</strong>';
    }
});
